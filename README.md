> ❔ Two additional documents were included after the publishing of this one, [one containing clarifications](CLARIFICATIONS.md) and other [a direct follow-up](FOLLOWUP_NEW.md) to this one. Please read both after reading this document, as it contains new information

> ❗ **CW: THIS DOCUMENT INCLUDES SENSITIVE TOPICS LIKE PEDOPHILIA AND GROOMING.**

> ❔ This document will include uncut screenshots whenever possible to provide as much context as possible. If any detail wasn't made clear, sections were poorly written, or if you want to send additional information, please send an email at owleyes@disroot.org or a DM at @owleyes@fe.disroot.org.

> ⚠️ Please take everything said in this document as neutrally and logically as possible. Pedophilia cases should be taken extremely objectively, and emotions can blind views and lead people to undesirable positions.

> ⚠️ Please **DO NOT HARASS** anyone mentioned in this document. Instead, consider creating constructive criticism and take everything as logical as possible, trying to find solutions to problems (if there's one). Harassment does not make any productive outcomes for any investigation.

> ⚠️ This document is **not aiming to mark someone as a bad person**. If you think I marked someone as one, please contact me so I can better rephrase what I said. The same applies if I tried to mark someone as a "good soul" in where I shouldn't.

> ⚠️ This document does not want to pick sides, instead if new evidence is shown, it will be analyzed by its finest details and see if it is enough to change the final statement. Do not trust this document and do not be scared to doubt about it and argue against or for it, or even ask for more details on specific parts. If you feel like I missed a point, please contact me.

# Study case #1: Faz o Pixie

This document aims to show what has been happening with Pixie, also known as JasPixie/PixieMeows/SorceressPixie, after they got exposed for allegations of grooming two minors, Adriimeow and Archie. This document also has the purpose of finding out if he had actually manipulated the minors, which was not made clear from the exposeds.

# 1 Terminology

- **"Pedophilia":** Although the original definition deviates from the current meaning (Pedo = children, philia = love, love for children. This does not have any sexual connotation), we'll use an approximate for the modern meaning of the word: "The adult that had lewd relationships with minors". This does not mean, however, that manipulation was involved, as the minor could have been aware of sexual topics before and is overall okay to do those actions.
- **"Grooming":** The act of encouraging minors into doing sexual actions with adults. It will be considered grooming if the minor had initially refused to do the actions and the adult insisted it or if the minor did not have any experience regarding to NSFW interactions. Other cases may be excluded or not, but it'll be explained.
- **Account names:** It will be referred as: 
  - **"Pixie"** to refer about the owner of "PixieMeows" and "JasPixie". Currently running [@pixie_blade](https://twitter.com/pixie_blade)
    - "**PixieMeows"** to talk about his SFW account 
      - **"PixieMeows exposed":** His exposed pointed at his SFW account
    - **"JasPixie"** to talk about his NSFW accounts, and **"SorceressPixie"** or **"LewdPixie"** to refer further NSFW accounts. 
      - **"JasPixie exposed":** will be used to talk about his first exposed. More information at [@JasPixieArchive](https://twitter.com/JasPixieArchive)
  - **"A user"** as the owner of this document and the project The Owl Eyes
  - **"Arrow"** as the artist [ArrowKnox](https://twitter.com/arrowknox)
  - **"Adri"** as the artist [Adriimeow](https://twitter.com/adriimeow_)
  - **"Archie"** for the user [Archie](https://twitter.com/archienyaa)
  - **"Fenku"** for the user [fenku](https://twitter.com/fenkupng)
  - **"Evie"** for the user [re.evie](https://twitter.com/peed_panties)
  - **"Mint"** for the artist [mintpaws](https://twitter.com/mintpaws)
  - **"Nathan"** for the artist and musician [Nathan. S. YT](https://twitter.com/NathanS_YT)
  - **"Cubusonic"** as the artist [Cubusonic](https://twitter.com/cubusonic)
  - **"Old Scratching Post":** Pixie and Cubusonic's server, called "Scratching Post". It was where both Pixie and Cubusonic's community stood before the drama 
    - **"Scratching Post 2":** Eventually, Pixie made a new server, initially invite-only, and its name made a reference towards Old Scratching post.

# 2 Goals

- Show any information related to Pixie. This is to give all context to the reader 
  - Interviews will not be censured at any cost and raw footage will be preferred, even if it exposes my opinions and thought process. Do consider that everything is temporal and opinions can change drastically
- Find out if Pixie groomed Adriimeow or Archie, or if all of the actions were consented by both parties without any incentive from him.

## 2\.1 Non-goals

- Invalidade or use JasPixie as argument, or any evidence shown at [@JasPixieArchive](https://twitter.com/JasPixieArchive) about JasPixie. Each case's diagnosis should be treated separately.
- Judge anyone's personality, or mark anyone as a bad person. The focus of this document is not to see if someone is a bad person, but to see if Pixie groomed Adri or Archie.
- Destroy anyone's reputation. This document has the exclusive intention to expose information, and **it does not hold responsibilities on anyone's reputation**, but unnecessary drama will be avoided.
- Analyze if he is a pedophile. From the evidence shown, it is clear that he had sexual interactions with minors, but this document has the point of analyzing what damage had Pixie caused, so focus is directed towards if he had manipulated the victims or not.
- Judge anyone by the law. It'll be considered that this happened on a "world without laws about pedophilia", to purely see the damage factors that the abuser has caused on the individual (excluding harm caused by the self reflection on the victim's side that is. It will be only taken the abuser-victim relationship as the only damage factors to be considered).

# 3\. Context

Pixie is a SFW and NSFW artist that had a history of interactions with minors. He was [exposed previously for NSFW interactions with minors](https://twitter.com/JasPixieArchive/status/1629921613666824194). After the first exposed, he decided to try out again with SFW art and got a good sized community (4k+ followers over on Twitter). Pixie also was a member at Mint's discord server, and that's how I (A user) met them. I previously saw his work over at JasPixie but i had no idea who they were at that time, and just found out about his NSFW past after PixieMeows exposed happened.

He also had a discord server that was a collab between Cubusonic (that was his partner until two weeks before the exposed), called Scratching Post. This one will be referred as "Old Scratching Post", as said before.

# 4\. The exposed

Arrow, the first one to make everything public, knew about Pixie's rocky past and actions, and have heard from a friend that he had been making them uncomfortable, and as such decided to tweet in his twitter circle to know about if he should address that, which is where he got more screenshots. He eventually published what did people told them about his current activity under [an exposed](https://x.com/arrowknox/status/1664796637066035201?s=20), but the issue is that the screenshots provided didn't prove if he had actually manipulated the victims or not, only had shown lewd interactions with heavy cropping.

> Attachment 4.1: Interview with ArrowKnox to know more about the context of the exposed

![](.attachments.2424/Screenshot_20230904_151518.jpg)

Fenku posted on Arrow's exposed [two screen recordings of lewd conversations that Archie and Pixie had together](https://twitter.com/fenkupng/status/1664837250390032384). Those confirmed that almost all screenshots given by ArrowKnox came from Archie, or at least a source that had those screenshots, and had shown the identity of one of his victims, as well as gave more context for the screenshots sent, but still didn't confirm any sexual interactions with.

Archie also gave more information on this tweet about his opinions on what happened, tho no screenshots were given.

> Attachment 4.2: Archie's opinions on what happened

![](.attachments.2424/Screenshot_20230925_190522_X.jpg)

Adri also made a tweet [showing more evidence that he had NSFW interactions with her, another minor](https://twitter.com/adriimeow_/status/1664990153763352576), tho it also suffered from the same issues as Arrow's exposed, as in, giving little evidence of manipulation.

After that, Pixie made a series of tweets (those weren't archived) showing regret from what he had done. He also confirmed that both Adri and Archie were actually his victims. Also he started a series of tweets exposing other pedophiles, but only one got exposed on it. He deleted all of the tweets related to on the next day and stood inactive on his account for about a month.

Also it is worth pointing about this pinned message over at Old Scratching Post where Pixie explained better the situation, tho that message was later on removed.

> Attachment 4.3: Pixie's response on Old Scratching Post

![](.attachments.2424/image-41%20%282%29.png)

# 5 Post-exposed

After his inactivity period, he privated his account and did not give any further explanation about the event, roughly saying "We know what happened, I do not need to explain further". The reason that he came back may have another reason. While he was running PixieMeows, he also was still running his NSFW account, SorceressPixie, but that account got suspended. He made another account, called LewdPixie, after the termination, that is also privated. Maybe he had plans to come back, but this ban maybe had inspired to return sooner

> Attachment 5.1: Pixie talking about SorceressPixie's ban

![](.attachments.2424/Screenshot_20230717-071602_Discord.jpg)

![](.attachments.2424/image-25-1.png)

![](.attachments.2424/Screenshot_20230717-090209_Twitter.jpg)

He didn't returned to do art over at PixieMeows, or at least it wasn't their focus, and decided to use that account as a way to post about his mental health status, as well as his other opinions. He also said that he was taking antidepressants to help with depression and his libido, tho it doesn't seem to be taking a long term effect on the libido part.

> Attachment 5.2: Pixie's antidepressants situation

![](.attachments.2424/DmLck8bz%20%282%29.jpg)

![](.attachments.2424/Screenshot_20230906_163545_X.jpg)

He also made a new discord server, that will be referred as Scratching Post 2

# 6 Scratching Post 2

This document will highlight some information that happened in this server, as it contains some crucial information about his case.

Also, fun fact: talking about his case was against the rules. Sometimes that rule was broken, but the rules in there are flexible.

> Attachment 6.0.1: Server's rules

![](.attachments.2424/Screenshot_20230904_070057%20%282%29.jpg)

## 6\.1 Joining dates

The server has existed for way longer than what has been initially thought. Assuming Pixie was the first member, the server was made at April 1 (unironically). Presumably this was a server only for him to send temporary stuff, but nothing proves that, it is unknown what he did with that server past June 3.

> 6\.1.1: Pixie's profile on the server. Profile picture and banner may not match exactly

![](.attachments.2424/Screenshot_20230910_192034.jpg)

Explaining the server structure, there were three important roles, Grass (role only used for Pixie), Bean town citizen (role for mods), and Chinatown citizen (role for other members). You could only talk on the server if you had the Chinatown role, that was manually given by the mods. The joining dates of some Bean town citizens, interestingly enough, dates as far as June 3, the day that Pixie got exposed. It is important to note that the server got public in August, so it is assumed that it was a friends server.

> Attachment 6.1.2: Some of the users with the Bean town citizens role

![](.attachments.2424/Screenshot_20230904_124936.jpg)

![](.attachments.2424/Screenshot_20230904_124928%20%282%29.jpg)

Also it is worth pointing out that Adri was a member on this server and was using as a server banner a sketch containing her OC and Pixie's OC, grass. She exited from the server at early September.

> Attachment 6.1.3: Adri's profile on Scratching Post 2

![](.attachments.2424/Screenshot_20230904_121907.jpg)

Some people have also joined Pixie's server, mainly Mint, Cubusonic and Nathan.

> Attachment 6.1.4: Cubusonic's profile at the server

![](.attachments.2424/Screenshot_20230916_002647.jpg)

Mint's joining, though, was explained on a [previous version on a now deleted document exposing Evie](https://cloud.uratania.ch/s/3Kp4DJ8KSWxFZBE), in where he says that he only joined for knowing about Evie's situation with Pixie.

> Attachment 6.1.5: Mint's profile at the server

![](.attachments.2424/Screenshot%20from%202023-09-09%2017-00-59%20%282%29.png)

Nathan's joining, tho, was explained when he first joined, showing that he had a friendship with Pixie. It is worth pointing out that it is unknown how is the friendship working as of now. Nathan may feel hate towards what Pixie have done, but have decided to still be friends with.

> Attachment 6.1.6: Nathan's joining + Friendship with Pixie

![](.attachments.2424/Screenshot_20230919_201038_Discord.jpg)

![](.attachments.2424/Screenshot_20230919_201044_Discord.jpg)

![](.attachments.2424/Screenshot_20230919_201049_Discord%20%285%29.jpg)

## 6\.2 Adri and Pixie

Adri's behavior with him was inconsistent considering that she was a victim of them. What I was expecting initially was to then never talk to him again, but apparently she was still keeping up contact with him after it. As shown in attachment 6.1.3, Adri was a member of the server and had a server banner with Pixie and her OCs.

Another evidence was a doodle posted by Adri on the server. It was posted on June 17, but it isn't know when it was drawn, this could pretty much have happened before all of the exposed.

> Attachment 6.2.1: Comfort doodle that Adri made.

![](.attachments.2424/Screenshot_20230904_084750.jpg)

There's also a creation of a modded Minecraft server made by Pixie that interestingly enough was called AdriPixie, tho it may have been a joke looking back at older messages of the server

> Attachment 6.2.2: Minecraft server's announcement

![](.attachments.2424/Screenshot_20230904_065959.jpg)

![](.attachments.2424/Screenshot_20230904_120519.jpg)

Another thing to add up was these doodle made by Pixie

> Attachment 6.2.3: Some doodles from Pixie

![](.attachments.2424/Screenshot_20230912_232419%20%282%29.jpg)

## 6\.3 Talk about Pixie's believes

There was also a talk about what he thinks about the topic of grooming. Although I agree on some points and I disagree with others, it is still important to show this, not because to show Pixie's opinion, but because there's crucial information about Archie

> Attachment 6.3.1: Conversation about grooming and pedophilia

![](.attachments.2424/Screenshot_20230910_002520.jpg)

![](.attachments.2424/Screenshot_20230910_003415.jpg)

![](.attachments.2424/Screenshot_20230910_005302.jpg)

![](.attachments.2424/Screenshot_20230910_010724%20%283%29.jpg)

![](.attachments.2424/Screenshot_20230910_011255.jpg)

If you've skipped all of this, here's a highlight on the information about Archie

> Attachment 6.3.2: Highlight about Archie

![](.attachments.2424/Screenshot%20from%202023-09-30%2000-07-36.png)

## 6\.4 Pixie and me talking about me being a spy in there

Pixie was aware of my existence in the server and always was suspecting that I was against them, which is not true, and made it clear. We had a conversation that, other than making me even more confused as to why I wasn't banned, exposed the possibility of both victims actually wanting lewd interactions with Pixie.

> Attachment 6.4.1: Conversation about me being the spy

![](.attachments.2424/Screenshot_20230911_213913.jpg)

Interestingly enough, I was not banned from the server after that.

# 7 Adri

I've also interviewed Adri to know more about her situation, but I did not want to make it clear initially. Sadly i cannot post any screenshots of the interview mainly because I've said to her that it wouldn't be published. Due to her response time, I couldn't extract much from. I'll give a summary of what happened:

1. I've asked if she was dating anyone above age 18, which she said no
2. I've asked if it was her first time getting groomed, which she said yes. I didn't asked if the groomer was Pixie, but it was implied that it was.
3. I've asked if she was still talking with Pixie. She initially said no, then was asked when she stopped talking, which she said it was the day that the exposed happened. I've told her that she should be truthful and that I knew about her interactions on the server. She said that she barely talks to him anymore. It is consistent with the frequency that she talked on the server, but still very weird considering that she was on the server and that there was AdriPixie's server
4. I've asked for pics that confirmed that she did get groomed by him, and not more ERP screenshots. She said she didn't want to look at those pics again, saying she does not want to remember any trauma, and as such I've decided to stop interviewing

This interview was insufficient to prove anything, and it just makes information more conflicting.

Also it is worth pointing out this conversation with an anonymous user

> Attachment 7.1: Conversation with anonymous user

![](.attachments.2424/obfuscated.png)

# 8 Archie

Although I didn't interview him, our interactions were pretty much very... wacky to say at least. This does not prove that what was said in attachment 4.3 is truthful, but it makes it much more trustworthy.

When I tried to interview him, he pretty much did not want to give any information about the interactions between Pixie and him, saying that it was out of my business. There's way more to the conversation, but it ended up with me just lamenting about me being obsessed with Pixie's case.

> Attachment 8.1: My first interactions with Archie

![](.attachments.2424/yZ0abIWm.jpg)

Some days after, I've decided to contact a common friend between us, that was also an adult, to know if Archie had any sexual interactions with them. I didn't get any response, but Archie came to me and said to not do that again. I've used the opportunity to talk about him wanting to come back at Pixie, and got a confusing response, but I assume he meant that he wanted to test him and see where things goes

After that, he asked if I was okay having sexual interactions with them. It is weird considering all of the things that I have done towards him, but I've accepted it regardless, tho taking screenshots to confirm that it was consented in case of something bad happening

> Attachment 8.2: Misread of Archie. This happened before she asked to be lewd, tho it happened on the same day. Maybe this is why they got comfortable with the idea?

![](.attachments.2424/Screenshot_20230911_210850.jpg)

> Attachment 8.3: When he asked about us having sexual interactions, as well as when he warned me to not DM friends for more info

![](.attachments.2424/Screenshot_20230911_205713%20%282%29.jpg)

![](.attachments.2424/Screenshot_20230911_210913%20%283%29.jpg)

After this event, we had more NSFW interactions, tho the third time that we had he asked to just stop talking to him, which I accepted

# 9 Evie

On September 21, Mint exposed an ex-friend of them called Evie for manipulative behavior. The reason for the exposed is out of scope for this document, but Evie, according to the document, had contacted adults, while being a minor, with the intention of "wanting to be groomed" as a sexual kink. She also contacted Pixie for presumably the same intentions, but got rejected by him. This could mean he is trying to change, or could mean that he was trying to avoid more trouble to him, as he was already skeptical about her reliability as Pixie was investigating on Evie at one point.

This information was deleted on a later revision of the document, [but the one containing it was archived](https://cloud.uratania.ch/s/tDD2e4reaQsj6gW)

> Attachment 9.1: Screenshot of Mint's document about Evie's attempt at having NSFW interactions with Pixie

![](.attachments.2424/Screenshot_20230926_111618_Word%20%283%29.jpg)

![](.attachments.2424/Screenshot_20230926_111629_Word%20%282%29.jpg)

> Attachment 9.2: Mint's document, showing a potential skepticism of Pixie over Evie

![](.attachments.2424/Screenshot%20from%202023-10-20%2021-36-28%20%283%29.png)

# 10 Misc

# 10\.1 Fake account

A fake account passing out as Pixie had been available for a short period of time, around early september. It was later on removed

> Attachment 10.1.1: Talk with me and someone that is aware of who is the owner of that fake account

![](.attachments.2424/Screenshot_20230912_234254.jpg)

## 10\.2 Confirmation that he is still running LewdPixie

> Attachment 10.2.1: Message sent on Scratcing Post 2 about Pixie's high interaction under his "locked account". This post was not seen on his SFW privated account, meaning that it was posted on another privated account, that being LewdPixie

![](.attachments.2424/Screenshot_20230906_130523%20%283%29.jpg)

# 10\.3 Pixie's negative experiences with Baraag

Baraag is a Mastodon instance that contained lolicon and cub. For more information about Mastodon and Federation, please check [fedi.tips](https://fedi.tips). This document won't go in detail about it, but Pixie had apparently joined that instance, **tho without knowing about the material in there**.

> Attachment 10.3.1: Initial talk about Mastodon, then mention about Baraag. We were explaining about the platform to them. They thought that they could only see content from one instance and that accounts were locked up on their own instances, a common mistake from newcomers

![](.attachments.2424/Screenshot_20230912_233358.jpg)

![](.attachments.2424/Screenshot_20230912_232242%20%283%29.jpg)

Looking up at @jaspixie on Baraag, it shows that the account is still existent, but it was made on May  24 of 2021, showing that this happened way before. The account is pretty much inactive, as he does not follow anyone from there, nor has any followers. The only interaction that the account shows is him boosting a post made on October 18 2022, which is odd considering his activity in there. Nothing comes up when searching for "@pixiemeows" or something simmilar

> Attachment 10.3.2: JasPixie's account at Baraag

![](.attachments.2424/Screenshot_20231015_101213_Kiwi%20Browser.jpg)

> Attachment 10.3.3: Translation of the boosted post

![](.attachments.2424/Screenshot_20231015_101634_Translate.jpg)

He had recently joined the fediverse under a new instance, but it won't provide his handler nor the instance name to avoid any Fediblocks from happening due to this.

# 11 Theorizing what happened

> ❗ This is just my opinions and analysis. Do not take everything here as truth

Taking everything in consideration, we can try to piece everything together:

Archie (according to attachment 4.3) has contacted Pixie after the JasPixie exposed, with the intention on helping them, but it can be presumed that they wanted lewd interactions from him (considering what was said in attachment 4.3 to be trustworthy, as well as section 8 making this information more believable), and Pixie got uncomfortable from it. Note that this could be far from the truth without knowing what happened in the DMs. It is unknown how did Arrow got the screenshots, or why they were leaked. A weird possibility is it could've been hate towards Pixie "completely forgetting about her", as shown in attachment 4.2 in "\[...\] they built this massive account and this massive community and completely forgot about me.". If Pixie forgot about them, then it wouldn't be a issue if he was uncomfortable with the situation. Note that this is just speculation, so this could be far from the truth. After some time, he DMed back to Pixie, wanting to return contact, and got ghosted (attachment 6.3.2).

Adri, on the other hand, has very little information on what happened. Due to her still ongoing interactions (attachment 6.1.3, section 6.2, and attachment 7.1), she is still talking with Pixie, even after what happened. It is not known about how it is going this interaction, but presumably they're okay with each other. But, we can still look back at her initial exposed:

1. Most of the attachments, specifically on the first ones, didn't gave context on what happened before the conversations. The possibility of Adri teasing Pixie instead of the other way is considerable, as we'll see on 4th numbered item.
2. On her exposed, the [third attachment at the first tweet](https://twitter.com/adriimeow_/status/1664990153763352576/photo/2) had shown that Adri actually drew NSFW art before, meaning that she already had interacted with lewd topics before, so Pixie's lewd interactions, by their own, didn't mold her drastically. This means that Adri was aware of the topic of sex and had actually got sexual enough by their own at the point of drawing NSFW art. In resume, Pixie did not introduce her to lewd topics. Still, the possibility of Pixie trying to groom her is open, as in, insisting on asking for things she is uneasy with or not stopping when Adri says to stop, among other things.
3. She also sent [an attachment of Pixie saying that he needs to protect her](https://x.com/adriimeow_/status/1664990159551512576?s=20), but it was made clear that he wanted to be an example for Adri, but he said that he didn't act like one, and asked help to her (indirectly) to see how he could be one. Also the attachment came with the message "but if I kept supporting him then he’d ask me for pics again tomorrow like nothing happened", but it wasn't sent proof of similar cases happening with her. As a positive point, this attachment contains previous context.
4. Also there's [this tweet, in which Pixie said that he has a problem of not stopping people when they tease him back](https://x.com/adriimeow_/status/1664990164106477570?s=20) and showing regret for that. But looking back at those attachments, specifically for the message "Of not stopping people once they began teasing", the possibility of Adri being the one that teasing him first is opened, because if he said that, then it could be implied that she wanted those interactions, although there's nothing to support that.
5. The [second to last tweet](https://twitter.com/adriimeow_/status/1664990171526299648) said that she let Pixie take advantage of her, but she did not explain how he took advantage. On the opposite side this could mean in the sense of "he took the advantage of me being already sexual", which in that case it depends from person to person to that argument be valid, but that's not grooming on its own (as in, it does not fit the definition purposed on this document). There's also her saying that she is only sexual with her partner, and that Pixie had made her to be sexual with him, even with that restriction, but again, no proof of him actually encouraging her to open that exception was given.

It is important to point out that her exposed was made in a quick and humble way, grabbing information to add up on Arrow's exposed, so those tweets should be taken as unclarified evidence than malicious intent, meaning that Adri needs to clarify each point said here. Adri, if you're reading this, do not take those as points against you, but points that you should clarify.

There's also Evie, which had contacted Pixie on August 20 to presumably get lewd interactions with them (attachment 9.1). She was exposed on Mint's document for having a kink towards pedophiles, yet Pixie avoided having lewd interactions interaction. It is not known what were the interactions before what that attachment had shown, but it can be assumed that he learnt his lesson from Arrow and Adri's exposeds, showing that he was avoiding teasing people back when he gets teased, but also this could mean nothing, as it is possible that he was skeptical about Evie (attachment 9.2).

To piece the whole story together, Arrow made a tweet on a twitter circle asking if he should talk about Pixie's past, and instead got screenshots of the actions. Later on, after Arrow making the exposed, Adri have decided to also tell her side, showing that Pixie also had NSFW interactions with her, thinking that she was manipulated by him. After that, Pixie had shown regret and stood on a hiatus, but made a friend server, in which Adri joined. Later on, he would return and private his account, and much time later he would open that friend server to the public, and make a Minecraft server. Archie had also tried to return contact, trying to lure him, but got ghosted instead. Later on, Evie would DM Pixie to get lewd interactions from them, but got rejected.

# 12 Conclusion

Considering all of the information gathered, **it is insufficient to determine if Pixie had groomed Adri or Archie**. It is still worth to point out that JasPixie's exposed is recent and has a lot of evidence proving that he pretty much groomed minors in the past, but isolation is important for each case, as variables, intentions and perspectives can change in either small or long time frames. After PixieMeows exposed, it seems that he is trying the best to avoid any NSFW interactions with minors and fix his libido issues, as shown in attachment 9.1 and in attachment 5.2, so there's hope for him stopping harming children, but again, things can change and a new exposed pop off. It is also worth pointing out that I am not trying to defend Pixie or go against him, and I am open for any clarifications or shifting opinions.

At the moment, to determine a conclusive answer, it is needed that either Archie or Adri provides evidence that shows that he had manipulated at least one of them, or if both were perfectly fine with having those interactions, without showing any doubt or uncertainty from the minors. For that, any of the victims would need to screenshot the conversations between them and Pixie and specific evidence of them getting manipulated, as well as what happened prior that. If evidence is given that he did groomed, then the only question left is why he preferred that path over not grooming.

To end this document. I want to leave a message that it is okay to be lewd with people, and that it is a way to cope with loneliness and avoid the necessity of having to date someone entirely for sexual relationships, but that it is both unnecessary and harmful to manipulate people into having sexual interactions. A good example of that would be if a minor said that he is not sure about doing lewd actions with the adult, but the adult replies encouraging to have. Also, if you see an exposed happening, please confirm if there is actual manipulation involved, and ask for screenshots of previous context. Context should be always understood for those cases, and every pedophilia case should not be generalized. If something happens to someone, doesn't mean the same will happen to another. A common trap that people fall for is to make assumptions and harass the one getting exposed, instead of trying to find a common solution for their issue and understanding their actions and see the overall context.

**__TL,DR:__** Read section 11