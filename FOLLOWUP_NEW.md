> ❔ This document is dependent on a [previous document](README.md?fileId=2424). Please read it before reading this one

> ❗ **CW: THIS DOCUMENT INCLUDES SENSITIVE TOPICS LIKE PEDOPHILIA AND GROOMING.**

> ❔ This document will include uncut screenshots whenever possible to provide as much context as possible. If any detail wasn't made clear, sections were poorly written, or if you want to send additional information, please send an email at owleyes@disroot.org or a DM at @owleyes@fe.disroot.org.

> ⚠️ Please take everything said in this document as neutrally and logically as possible. Pedophilia cases should be taken extremely objectively, and emotions can blind views and lead people to undesirable positions.

> ⚠️ Please **DO NOT HARASS** anyone mentioned in this document. Instead, consider creating constructive criticism and take everything as logical as possible, trying to find solutions to problems (if there's one). Harassment does not make any productive outcomes for any investigation.

> ⚠️ This document is **not aiming to mark someone as a bad person**. If you think I marked someone as one, please contact me so I can better rephrase what I said. The same applies if I tried to mark someone as a "good soul" in where I shouldn't.

> ⚠️ This document does not want to pick sides, instead if new evidence is shown, it will be analyzed by its finest details and see if it is enough to change the final statement. Do not trust this document and do not be scared to doubt about it and argue against or for it, or even ask for more details on specific parts. If you feel like I missed a point, please contact me.

# Follow-up to Study case #1

This document aims to give more information about Pixie, also known as JasPixie/PixieMeows/SorceressPixie, that has been given to me after publishing the document. This documents every event since October 27 2023 to this day (December 2 2023).

# 1 Terminology

## 1\.1 Extra Terminology

- **Account names:** It will be referred as: 
  - **"Skrittle"** as the artist [Skrittle_Pop](https://twitter.com/Skrittle_Pop)

# 14 DMs with Pixie

## 14\.1: Twitter

After publishing my document, I've decided to contact Pixie due to me not having much options left. I did on Twitter because I thought I would have a better luck at contacting him over there at that time.

> Attachment 14.1.1: DM with Pixie over at Twitter

![](.attachments.5517/Screenshot_20231106_150131_X.jpg)

It cannot be confirmed if what he's saying is truthful or not, but also there's no evidence that suggests that he was lying previously, as well as he has no reason to lie considering that he had shown regret for his actions, as shown in section 4

This also shows that the traumas that both have weren't made by Pixie, or even were worsened by him, in some way he was also trying to help by pushing them away when his libido was not high and noticed that any of the victims wanted ot interact lewdly with.

## 14\.2 Discord DMs

Hours after the talk, Pixie DMed me on Discord asking me a favor, and after that gave me more information. It contains personal information from both victims, specifically on Adri's side that I've decided to not share for the safety of both, so I won't be publishing the full conversation. But here are some highlights:

> Attachment 14.2.1: His answer towards section 6.3

![](.attachments.5517/Screenshot%20from%202023-11-11%2001-23-54.png)

> Attachment 14.2.2: His answer towards Evie's situation + Some extra confirmations related to Adri and the Minecraft server

![](.attachments.5517/Screenshot%20from%202023-11-11%2001-29-07.png)

> Attachment 14.2.3: Pixie's view on the harassment that Adri's ex did to him

![](.attachments.5517/Screenshot%20from%202023-11-11%2001-40-54.png)

# 15 Skittle's art

Around November 25, the artist Skittle made an fanart of Jasmine. It was removed from Twitter, but Skittle sent me the art

> Attachment 15.1: The art that Skittle made

![](.attachments.5517/pixe.png)

After publishing, 4 users, including the artist Optical, contacted Skittle to warn about Pixie and what he had done. He was aware of the accusations, but didn't knew about the screenshots. Eventually, he would get suspicious publicly towards Pixie, but he would eventually return to the community, being sorry about it. I've did an interview with him, but he said that he did not want to it be public.

# 16 Nathan joins the drama

On November 26, presumably after Skittle's art was published, Nathan made [this post](https://twitter.com/NathanS_YT/status/1728905091917426965).

> Attachment 16.1: Nathan's post about Pixie

![](.attachments.5517/Screenshot%20from%202023-11-26%2021-32-40.png)

This post does not add any information upon the already existing drama and only revisits drama that was already solved. It also contains a lot of typos, showing that it was rushed. I'll analyze this response bit by bit:

1. "If you support @pixie_blade, unfollow me"

   It is up to the viewer to determine if they want to unfollow you. The only thing that this accomplishes is making your followers have the same mindset as you. Also you're dependent on other people on your career, meaning that excluding people to follow you will affect your career.
2. "I don't have any clue or idea how they got back into the community like this scot-free"

   He didn't. He would get banned in most communities frequently due to the drama, and still has issues of getting banned in some to this day. The image below shows a recent ban on one of the servers. Also, what other path would they take? It wouldn't change nothing if they went to another account, and they would rather continue on the artistic career.

   > Attachment 16.2: Pixie getting banned on one of the servers

   ![](.attachments.5517/eeee.jpg)
3. "and only recently got exposed again about 5 months ago for grooming 2 minors"

   I've debunked the accusations, showing that the grooming evidence wasn't obvious, and without basis. Read section 11 for more.
4. "saying they were going to impregnated the victim, which I actually find is disgusing behavour that shouldn't be allowed AT ALL"

   Impregnation mentions are normal in pornographic material and on erotic roleplays and are a way to tease the victim even more, as it is understood as a peak of the sexual act. If it is not allowed, then pornographic material containing an ovum getting penetrated by a sperm "shouldn't be allowed AT ALL".

   Also I do not understand why that action would harm directly the victim, yes it may be "disgusting behavior" but does it damage the minor directly?
5. "You have to understand that even though Pixie is saying 'They've forgiven me' isn't gonna fix anything"

   It won't, and you're right, but it means that the drama is done and as such it should be left alone. Bringing it to discussion will only return drama that has already been solved and aren't in need of discussion.
6. "that the vicims now have to deal with trauma that was put on them from this for probably the rest of their life."

   Pixie didn't traumatized them. I've already debunked the accusations on section 11, meaning that the basis on the accusation of trauma is false. The traumas that both have weren't caused by Pixie, nor were worsened by.

Although this did absolutely add nothing, it made people repost Arrow's exposed, adding more fire to Pixie's reputation

> Attachment 16.3: Retweet that came from an artist called "Lea"

![](.attachments.5517/image%20%283%29.png)

# 17 Pixie replies publicly about

Eventually, Pixie would respond to the drama that had returned

> Attachment 17.1: Pixie's thread about the drama

![](.attachments.5517/Screenshot_20231127_174313_X.jpg)

Although it does not introduce anything new to the table, it answers as to why he never gave an statement towards the issue and explained some misconceptions that were being sent at that time.

Eventually, Nathan would reply to that post:

> Attachment 17.2: Nathan's reply

![](.attachments.5517/Screenshot_20231127_174245_X.jpg)

![](.attachments.5517/20231127_174258.jpg)

It is ironic that Nathan would argue that his case is different from Pixie because he was framed as a groomer and that later on the ones that exposed him would apologize for, but also not consider that Pixie was also framed as a groomer and the victims apologized for the exposed, as shown in attachment 14.1.1. The only case in where I would see difference is if there was no proof to back it up the claim that Nathan had groomed anyone.

# 18 The victims speak

## 18\.1 Archie

Eventually, on November 28, Archie would make a small twitter thread exposing Pixie with material that incriminates him as a manipulator... Finally.

> Attachment 18.1.1: Archie's thread

![](.attachments.5517/Screenshot_20231128_131513_X.jpg)

![](.attachments.5517/20231205_220024.jpg)

I will debunk their accusations:

1. In his thread, he spoken with Pixie about what happened with Adri and got a "you don't understand true friendship" message. That makes sense, because Archie was the one that made the interactions public, and as such it means a break of trust and support, and "understanding true friendship" is pretty much related to trust and support. Meanwhile, Adri actually tried to help Pixie, and stood on his side for the entirety of the happenings, a.k.a. trusted and supported.
2. He also says that Pixie viewed Adri as the hope to help with his mental health (and considering that Adri practically motivated him to find professional health, the view is pretty justifiable), but also that he forgot that Archie was the one that made both meet each other... That doesn't mark Pixie guilty, at most it is a request for a thanks.
3. He said that Pixie took advantage of several people, and used the fact that Pixie offers gifts as proof of that manipulation. It came with an attachment that have a conversation that they had, which contained an image of a drawing that Pixie did, that he gave the title of "The only things that matter". That's the highest point of the exposed, yet gifts as grooming accusation is a weak argument. It either could be actually genuine feelings or an attempt at hooking Archie, and that ambiguity marks the evidence as a very weak one. To resolve that, Archie would need to send an attachment of Pixie actually requesting mandatory return of support, which is not visible from the image. To explain better this argument, take the example of me wanting to gift a friend that helped me while i was in a depressive state. This is pretty much the case here, because Archie tried to help Pixie, and as such he decided to gift him
4. It was said that Pixie soft blocked him while venting to Adri, and that it was a recommendation from her to block him. It makes me to wonder about what conversation did both (Pixie and Archie) have at the time between the art and the block, but that's beside the point. It shows that both victims have a hostility between each other.
5. At the end, Archie calls himself as the main victim. That's interesting, mainly because he views himself as the main person in the exposed, where in actually both play an equal role in the case. Maybe that could be viewed as selfish?

At the end, close to when Archie gave his words, Pixie would get suspended on Twitter.

## 18\.2 Adri

On the other day, Adri would say her side towards the issue, which denied any involvement with Pixie after the exposed.

> Attachment 18.2.1: Adri's response

![](.attachments.5517/Screenshot_20231128_150340_X.jpg)

After asking Pixie about, he replied saying that they are still friends.

> Attachment 18.2.2: Pixie's confirmation that they're still friends

![](.attachments.5517/image.png)

She also retweeted this post

> Attachment 18.2.3: Adri's retweet

![](.attachments.5517/Screenshot_20231205_223253_X.jpg)

One of the things that was said by Strafely was that he consoled Adri on numerous occasions, but it is not told how, why and examples of that happening.

Archie replied to that tweet, and said that he was aware of the patterns and behaviors of groomers. He may be more qualified to determine who's a groomer and who's not, but it doesn't excuse the "gift" argument that he used, and could even be argued that it may be a miscalculation. Strafely replied affirming that groomers return to their victims hurting them even more. I've already shown time and time again from this document that it is not clear how did he hurt both victims. At the end, Archie says that "Pixie can perish for all I care".

# 19 Public Apologies

I should be honest and say that I am sorry for being too harsh on both Archie and Adri on my analysis over on section 11.

In Archie's case, he was pretty much dealing with someone that had a bad reputation accused of being manipulative, so him being put aside from Pixie's part was worrying from him, added up to Pixie getting "better" while being in a problematic community (according to Pixie itself), and as such when Arrow opened the possibility of getting him exposed, he may have thought it was a good idea to get a "revenge" or a callout to him, and as such later on he would feel regret. This is my speculation tho, but it is understandable him action. I am also sorry for accepting doing the erotic roleplays, as I should be more responsible and not let my libido accept your offering, and as such I will be more responsible towards my actions, even if it was you that asked. But I am not sorry about breaking my promise of not putting you in the situation back. As shown in attachment 18.1, you were totally willing to return to the drama and not try to solve your issues with Pixie, as in, affirming if you want to break up contact for example. I wouldn't throw the possibility that you tried, but still, exposing him for a bogus action is not an healthy way to finish a friendship. Instead, you could do what you did with me and ask him to stop contact, if you wanted that is. Still, I am open hands if you want to clarify or give me extra information (even tho you will probably not give me anything).

In Adri's side, according to Pixie, she was panicking with him when Arrow's twitter thread came, and thought it was for the best of the two to give her side, worried by the thought that she was groomed. So her twitter thread was pretty much done in panic, and not done clearly. I've tried to overanalyze every word given in there to show how poorly and rushed it was made, but it was more of a call to see if she replied anything about. I knew that I wouldn't get the information out of her, so I tried to see if showing those points would make her to say anything, not even a simple "He hadn't manipulated me and encouraged me to do those", and not gonna lie it was stupid. Now that I know why she did not want to talk or show to the light that she is still talking to him, I've made this follow-up to try to fix the bombshell that I created, and again, I do not think that talking with him is wrong, but that not being transparent and making others to think that he hurt you is a bad thing in my opinion. I hope we can get in a more peaceful matter now. Please **DO NOT HARASS** her for still talking to him, it is ultimately their decision to do it, and by harassing it will degradate her mental health.

I should also say sorry for Pixie to still be diving deep in his case. I know that he is tired of that and that he wants to move on. The reason that I am still deciding to push fowards is to break up with the narrative that was put in him, and that he is avoiding to get it resolved, mainly because it is useless to him still try to explain, because people won't believe in him anyway. Still, this will be my last time that I will write a document about him, until a new accusation with new victims appears or if new information appers that changes the entire story.

# 20 Piecing everything together

> ❗ This is what I think on what happened, and as such it will likely diverge from reality. Please do not take everything in this section as truthful

On his childhood, Pixie was groomed constantly at the point of turning into normality, and due to that he got extremely hypersexual and interacted heavily into NSFW places. Due to his experience, he pretty much brought his past experiences to others, grooming some victims in the process. Eventually, he would get exposed for his actions over at the Archive account (originally named "JasPixie exposed" on my initial document).

Due to that, Archie, feeling sympathy, contacted Pixie offering help. The thing is that both got sexual with each other, but more from Archie's side that part, and Pixie wasn't encouraging it and even sometimes discouraging him from doing those, but the interactions happened due to Pixie having libido issues, gotten from when he was groomed. A similar story happened with Adri.

Eventually, Pixie would move on and put Archie aside, and eventually interact more with his community, which according to him was full of problems and superficial people. Due to that, when Arrow made a twitter circle wanting to know if he should talk about Pixie, Archie gave him information about, and as such Arrow would made a twitter thread exposing Pixie.

Both Adri and Pixie, after that, were panicking after the thread being published. Due to that, she felt pressured and conflicted to talk about her side, and as such she made another twitter thread exposing him again, showing some erotic roleplays that both had.

Eventually, Pixie would get in a hiatus state for about a month and get treatment for his situation, and would get back after his NSFW main getting suspended. He would later on create another, but it got suspended as well. He did not explained anything more after that, and as such decided to return to his SFW main, returning to post art after some time.

Five months have passed since the drama started and Skittle meets with Pixie, and eventually draws a fanart. Hours after, four people, including Optical, would contact Skittle to warn about Pixie. Eventually Nathan would make a post that did not add anything to the exposed, probably after seeing Skittle's art, and this made the drama to return to light.

After all of that, Archie would make a thread exposing Pixie for manipulation, but with the only evidence being an art gift. At then end, Pixie's twitter account would be suspended.

# 21 Conclusion

Assuming that everything told in 14.1 is truthful, **it is sufficient to say that Pixie did not groomed Archie and Adri**, and was from their own free unbiased will to do the actions.

This story tells a side of drama where even if someone tries to move on, people will not forget and will still harass for your actions, even if you fix your issues. It is proof that drama is always eternal, even if the issues get fixed. Doubt and untrust plays a huge role, and as such statements such as "I've fixed my issues" gets thrown to the trash to push a narrative of someone being a bad person eternally, using other examples with people that didn't fixed themselves as reasons to still push it. Logic gets thrown to the trash in favor of maintaining a harmful image, and to not get punished, everyone has to push it fowards, anything that's remotely supportive is quickly ignored and pushed down. This is also harmful for the victims, as if they regret to what they did, they get too scared to try to change and explain.

To end this document, I want to show a conversation in the Scratching Post 2, before everything blew up:

> Attachment 21.1: Empathy

![](.attachments.5517/Rmkel8fb.jpg)

TL;DR: Read everything, don't be ignornat