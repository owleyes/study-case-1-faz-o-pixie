> ❔ This document is dependent on a [previous document](README.md). Please read it before reading this one

> ❗ **CW: THIS DOCUMENT INCLUDES SENSITIVE TOPICS LIKE PEDOPHILIA AND GROOMING.**

> ❔ This document will include uncut screenshots whenever possible to provide as much context as possible. If any detail wasn't made clear, sections were poorly written, or if you want to send additional information, please send an email at owleyes@disroot.org or a DM at @owleyes@fe.disroot.org.

> ⚠️ Please take everything said in this document as neutrally and logically as possible. Pedophilia cases should be taken extremely objectively, and emotions can blind views and lead people to undesirable positions.

> ⚠️ Please **DO NOT HARASS** anyone mentioned in this document. Instead, consider creating constructive criticism and take everything as logical as possible, trying to find solutions to problems (if there's one). Harassment does not make any productive outcomes for any investigation.

> ⚠️ This document is **not aiming to mark someone as a bad person**. If you think I marked someone as one, please contact me so I can better rephrase what I said. The same applies if I tried to mark someone as a "good soul" in where I shouldn't.

> ⚠️ This document does not want to pick sides, instead if new evidence is shown, it will be analyzed by its finest details and see if it is enough to change the final statement. Do not trust this document and do not be scared to doubt about it and argue against or for it, or even ask for more details on specific parts. If you feel like I missed a point, please contact me.

# Clarifications to Study case #1

This document aims to clarify some things about my document about Pixie, also known as JasPixie/PixieMeows/SorceressPixie. This is also a more personal document, and as such I'll be referring myself a lot in this document. More stuff happened since the exposed, but I'll address those on another document. Nomeclatures will be returned from my first document.

# 13 Clarifications

## 13\.1 LewdPixie

I've said on section 5 that Pixie made a new account after his NSFW account got suspended, and it was called LewdPixie. That account no longer exists as it was suspended by Twitter. This means that the attachment 10.2.1 is actually invalid confirmation. At that time I did not have reflected well enough about it and did not have access towards Pixie's account, that was private at that time. Due to me assuming that the account was still active, and me assuming that he was only using Jasmine (his Sylveon-based OC, Grass is the Sprigatito-based one) for drawing NSFW art, I assumed that him showing a post of a recent drawing of Jasmine meant that he posted it on LewdPixie, which is not true. At that time I was getting some of the posts on Pixie's account (while it was privated) from a third party, so I couldn't analyze by my own the existence of the files.

## 13\.2 One of the rules of Scratching Post 2

At the start of section 6, one of the rules said that talking about his drama was banned. This rule was put to avoid unnecessary unproductive drama happening on the server, and talking about it was allowed, and oftenly was done. I should have made more clear about how it was handled the drama in there.

## 13\.3 Nathan and Pixie

I've also exposed some messages from Scratching Post 2 that have shown that Nathan and Pixie were still friends after the exposed. After publishing the document, I got told by Nathan that Pixie had blocked him while Mint's drama was happening. This attachment came with it.

> Attachment 13.3.1: The attachment that Nathan gave me

![](.attachments.5429/fJZHdaHj.png)

I've contacted Pixie to know why did he blocked him, getting this as a response:

> Attachment 13.3.2: Pixie's reasons to block him

![](.attachments.5429/Screenshot_20231116_204730.jpg)

It is worth pointing out that it isn't known if Pixie's allegations are true (also it is outside of this document frankly), but it is an old case, so intentions and mindset may have been changed. Also I should answer as to why Nathan may be an interesting piece to look at, although unrelated to it:

Nathan is a well known member on Mint's old discord server community and has been interacting with Pixie's exposed since it came to light, on [Arrow](https://twitter.com/NathanS_YT/status/1664831320642420738) and on [Adri](https://twitter.com/NathanS_YT/status/1664994870358843394)'s tweets, as well as [when Pixie returned](https://x.com/NathanS_YT/status/1680784909596540929?s=20). It would be expected of him not interacting with Pixie after all of that, but as proven in Attachment 6.1.6, he was still talking to him. This matters as this spreads negatively Pixie's drama, and as such it causes more damage to his already broken image.

Pixie unblocked Nathan after some time tho.

> Attachment 13.3.3: Pixie's mention on unblocking Nathan

![](.attachments.5429/Screenshot%20from%202023-11-16%2021-20-20.png)